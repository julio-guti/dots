export EDITOR="nvim"
export TERMINAL="urxvtc"
export BROWSER="chromium"

# Go
export GOPATH="$HOME/.go"
export CGO_CXXFLAGS_ALLOW=".*"
export CGO_LDFLAGS_ALLOW=".*"
export CGO_CFLAGS_ALLOW=".*"
export QT_PKG_CONFIG=true

export PATH="$PATH:$HOME/.local/bin:$GOPATH/bin"

[ -f ~/.bashrc ] && source ~/.bashrc

# Start graphical server if i3 not already running.
if [ "$(tty)" = "/dev/tty1" ]; then
	pgrep -x i3 || exec startx
fi

wal -Rn
