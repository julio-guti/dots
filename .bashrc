# If not running interactively, don't do anything
[[ $- != *i* ]] && return

stty -ixon # Disable ctrl-s and ctrl-q.
shopt -s autocd #Allows you to cd into directory merely by typing the directory name.
HISTSIZE= HISTFILESIZE= # Infinite history.

PS1='[\u@\h \W]\$ '

alias vim='nvim'
alias bc='bc -qli'
alias grep='grep --color=auto'
alias ls='ls --color=auto'

if [ -f /work/bashrc ]; then
	alias sw='source /work/bashrc'
fi

